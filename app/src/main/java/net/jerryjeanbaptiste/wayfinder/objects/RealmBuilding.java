package net.jerryjeanbaptiste.wayfinder.objects;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jerry1 on 4/19/15.
 */
public class RealmBuilding extends RealmObject {
    private String BuildingName;
    private String BuildingDescription;
    private String Image;
    private RealmList<RealmDepartment> departments;

    public String getBuildingName() {
        return BuildingName;
    }

    public void setBuildingName(String buildingName) {
        BuildingName = buildingName;
    }

    public String getBuildingDescription() {
        return BuildingDescription;
    }

    public void setBuildingDescription(String buildingDescription) {
        BuildingDescription = buildingDescription;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public RealmList<RealmDepartment> getDepartments() {
        return departments;
    }

    public void setDepartments(RealmList<RealmDepartment> departments) {
        this.departments = departments;
    }
}
