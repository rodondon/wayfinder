package net.jerryjeanbaptiste.wayfinder.objects;

/**
 * Created by Jerry on 2/14/2015.
 * This is the representation of a building on campus
 */
public class Building extends Vertex{


    public Building (RealmVertex vertex)
    {
        super(vertex.getName());
    }
    public Building(String BuildingName) {
        super(BuildingName);
    }
}
