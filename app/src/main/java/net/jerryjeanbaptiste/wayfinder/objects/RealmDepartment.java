package net.jerryjeanbaptiste.wayfinder.objects;

import io.realm.RealmObject;

/**
 * Created by jerry1 on 4/19/15.
 */
public class RealmDepartment extends RealmObject {
    private String DepartmentName;
    private String DepartmentPhone;
    private String DepartmentDescription;
    private String Image;
    private String buildingName;

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String building) {
        this.buildingName = building;
    }

    public String getDepartmentName() {
        return DepartmentName;
    }

    public void setDepartmentName(String departmentName) {
        DepartmentName = departmentName;
    }

    public String getDepartmentPhone() {
        return DepartmentPhone;
    }

    public void setDepartmentPhone(String departmentPhone) {
        DepartmentPhone = departmentPhone;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getDepartmentDescription() {
        return DepartmentDescription;
    }

    public void setDepartmentDescription(String departmentDescription) {
        DepartmentDescription = departmentDescription;
    }
}
