package net.jerryjeanbaptiste.wayfinder.ui;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import net.jerryjeanbaptiste.wayfinder.R;
import net.jerryjeanbaptiste.wayfinder.objects.AlarmReceiver;
import net.jerryjeanbaptiste.wayfinder.objects.RealmBuilding;
import net.jerryjeanbaptiste.wayfinder.objects.RealmDepartment;
import net.jerryjeanbaptiste.wayfinder.objects.RealmVertex;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class HomeActivity extends ActionBarActivity {

    Spinner buildingsSpinner;
    Spinner departmentsSpinner;
    Button goToBuildingButton;
    Button goToDepartmentButton;
    Button goToMapButton;
    Button goToScheduleButton;
    Button goToRouteRequestButton;
    Realm realm;
    SharedPreferences prefs = null;
    private PendingIntent pendingIntent;
    public static final String TEST_ACTION= "net.jerryjeanbaptiste.wayfinder.TEST_ACTION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Log.d("HOME ACTIVITY", "inside homeactivity");

        /*--------------------ALARM-------------------------------*/

//        Intent test = new Intent();
//        test.setAction(TEST_ACTION);
//
//        sendBroadcast(test);

//        Intent myIntent = new Intent(HomeActivity.this, MyReceiver.class);
//        pendingIntent = PendingIntent.getBroadcast(HomeActivity.this,123456789, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
//
//        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
//        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        /*-------------------------------ALARM(END)---------------------------------*/

        prefs = getSharedPreferences("net.jerryjeanbaptiste.wayfinder", MODE_PRIVATE);
        loadData();

        loadViews();
//        int count = realm.where(RealmVertex.class).findAll().size();
//        RealmVertex test = realm.where(RealmVertex.class).findAll().get(2);
//        Toast.makeText(this, Integer.toString(count)+ " "+test.getAdjacencies().get(0).getTarget() ,Toast.LENGTH_LONG).show();

        RealmQuery<RealmBuilding> buildingRealmQuery = realm.where(RealmBuilding.class);
        RealmResults<RealmBuilding> buildingRealmResults= buildingRealmQuery.findAll();
        buildingRealmResults.sort("BuildingName");

        RealmQuery<RealmDepartment> departmentRealmQuery = realm.where(RealmDepartment.class);
        RealmResults<RealmDepartment> departmentRealmResults=departmentRealmQuery.findAll();
        departmentRealmResults.sort("DepartmentName");


        ArrayAdapter<String> buildingListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        buildingListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        for(RealmBuilding item : buildingRealmResults)
        {
            buildingListAdapter.add(item.getBuildingName());
        }
        buildingsSpinner.setAdapter(buildingListAdapter);

        ArrayAdapter<String> departmentListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        departmentListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        for(RealmDepartment item : departmentRealmResults)
        {
            departmentListAdapter.add(item.getDepartmentName());
        }
        departmentsSpinner.setAdapter(departmentListAdapter);

        setButtonListeners();


    }
    public void loadViews()
    {
        buildingsSpinner = (Spinner) findViewById(R.id.buildingsSpinner);
        departmentsSpinner = (Spinner) findViewById(R.id.departmentsSpinner);

        goToBuildingButton = (Button) findViewById(R.id.viewBuildingButton);
        goToDepartmentButton = (Button) findViewById(R.id.viewDepartmentButton);
        goToMapButton = (Button) findViewById(R.id.homeMapButton);
        goToRouteRequestButton = (Button) findViewById(R.id.homeRequestDirectionsButton);
        goToScheduleButton = (Button) findViewById(R.id.homeScheduleButton);
    }

    public void loadData()
    {
        realm = Realm.getInstance(this);
        if (prefs.getBoolean("firstrun", true)) {
            prefs.edit().putBoolean("UserLoggedIn", false).commit();
            InputStream is = getResources().openRawResource(R.raw.data);
            realm.beginTransaction();
            try {
                realm.createAllFromJson(RealmBuilding.class, is);
                realm.commitTransaction();
                is.close();
            } catch (IOException e) {
                realm.cancelTransaction();
            }
            realm.beginTransaction();
            InputStream is2 = getResources().openRawResource(R.raw.coord);
            try {
                realm.createAllFromJson(RealmVertex.class, is2);
                realm.commitTransaction();
            } catch (IOException e) {
                realm.cancelTransaction();
            }
            realm.beginTransaction();
            RealmResults<RealmBuilding> buildings = realm.where(RealmBuilding.class).findAll();
            for(int i = 0;i<buildings.size();i++)
            {
                for(int b=0;b<buildings.get(i).getDepartments().size();b++)
                {
                    buildings.get(i).getDepartments().get(b).setBuildingName(buildings.get(i).getBuildingName());
                }
            }
            realm.commitTransaction();
            prefs.edit().putBoolean("firstrun", false).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id==R.id.schedule_page)
        {
            Intent intent= new Intent(this, ScheduleActivity.class);
            startActivity(intent);
        }
        else if(id==R.id.Building_page)
        {
            Intent intent = new Intent(this,BuildingActivity.class);
            startActivity(intent);
        }
        else if(id== R.id.Admin_Home_page)
        {
            Intent intent= new Intent(this,LoginActivity.class);
            startActivity(intent);
        }
        else if(id== R.id.Edit_Building_page)
        {
            Intent intent = new Intent(this,EditBuildingActivity.class);
            startActivity(intent);
        }
        else if (id==R.id.Department_page)
        {
            Intent intent = new Intent(this, DepartmentActivity.class);
            startActivity(intent);
        }
        else if(id== R.id.Edit_Pathway_page)
        {
            Intent intent = new Intent( this, EditPathwayActivity.class);
            startActivity(intent);
        }
        else if(id== R.id.Schedule_page)
        {
            Intent intent = new Intent (this, ScheduleActivity.class);
            startActivity(intent);
        }
        else if ( id== R.id.Register_page)
        {
            Intent intent = new Intent (this, Register.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void setButtonListeners()
    {
        goToBuildingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goToBuildingIntent = new Intent(HomeActivity.this, BuildingActivity.class);
                buildingsSpinner.getSelectedItemPosition();

                String text = ((Spinner)findViewById(R.id.buildingsSpinner)).getSelectedItem().toString();

                goToBuildingIntent.putExtra("name",text);

                startActivity(goToBuildingIntent);

            }
        });

        goToDepartmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goToDepartmentIntent = new Intent(HomeActivity.this, DepartmentActivity.class);
                departmentsSpinner.getSelectedItemPosition();

                String text = ((Spinner) findViewById(R.id.departmentsSpinner)).getSelectedItem().toString();

                goToDepartmentIntent.putExtra("name",text);

                startActivity(goToDepartmentIntent);

            }
        });

        goToMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goToMapIntent = new Intent(HomeActivity.this, MapActivity.class);

                startActivity(goToMapIntent);

            }
        });

        goToScheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goToScheduleIntent = new Intent(HomeActivity.this, ScheduleActivity.class);

                startActivity(goToScheduleIntent);

            }
        });

        goToRouteRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goToRouteRequestIntent = new Intent(HomeActivity.this, MainActivity.class);

                startActivity(goToRouteRequestIntent);
            }
        });


    }

}
