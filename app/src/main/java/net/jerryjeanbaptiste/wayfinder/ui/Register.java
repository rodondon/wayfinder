package net.jerryjeanbaptiste.wayfinder.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.jerryjeanbaptiste.wayfinder.R;
import net.jerryjeanbaptiste.wayfinder.objects.RealmUser;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.realm.Realm;
import io.realm.exceptions.RealmException;

public class Register extends ActionBarActivity {

    EditText email,username,password,repeatPassword;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        loadViews();
        addListeners();


    }

    private void addListeners()
    {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email_text= email.getText().toString();
                String username_text=username.getText().toString();
                String password_text = password.getText().toString();
                String repeatPassword_text= repeatPassword.getText().toString();
                if(email_text.length()>3 )
                {
                    if(username_text.length()>3)
                    {
                        if(password_text.length()>4)
                        {
                            if(repeatPassword_text.equals(password_text))
                            {
                                Realm realm = Realm.getInstance(Register.this);
                                realm.beginTransaction();
                                RealmUser user = realm.createObject(RealmUser.class);
                                try {
                                    user.setEmail(email_text);
                                }
                                catch (RealmException e)
                                {
                                    realm.cancelTransaction();
                                    Toast.makeText(Register.this,"This account already exists", Toast.LENGTH_LONG).show();
                                    e.printStackTrace();
                                    return; //creating account failed, quit now.
                                }
                                user.setUsername(username_text);
                                //Encrypt and Store password
                                String hash=sha256(password_text,Register.this);
                                if(hash!=null)
                                    user.setPassword(hash);
                                else
                                {
                                    realm.cancelTransaction();
                                    return; //creating account failed, quit now.

                                }

                                realm.commitTransaction();

                                Toast.makeText(Register.this, "Registration Successful", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(Register.this, ScheduleActivity.class);
                                startActivity(intent);

                            }
                            else
                            {
                                Toast.makeText(Register.this, "Your passwords should be identical", Toast.LENGTH_LONG);
                            }
                        }
                        else
                        {
                            Toast.makeText(Register.this,"Invalid password. More than 4 characters needed", Toast.LENGTH_LONG).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(Register.this,"Invalid Username",Toast.LENGTH_LONG).show();
                    }

                }
                else
                    Toast.makeText(Register.this,"Invalid Email", Toast.LENGTH_LONG).show();

            }
        });
    }
    //Encrypt a string with the SHA-256 algorithm, context is for error messages
   public static String sha256(String in,Context context)
    {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] b = in.getBytes(Charset.forName("UTF-8"));
            digest.update(b);
            return new String(digest.digest(), "UTF-8");
        } catch (NoSuchAlgorithmException e) {
            Toast.makeText(context,"There was an error",Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            Toast.makeText(context,"There was an error",Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return null;
    }
    public void loadViews()
    {
        email = (EditText) findViewById(R.id.emailEditText);
        username = (EditText) findViewById(R.id.usernameEditText);
        password = (EditText) findViewById(R.id.passwordEditText);
        repeatPassword = (EditText) findViewById(R.id.repeatPasswordEditText);
        submit = (Button) findViewById(R.id.RegisterSubmitButton);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
