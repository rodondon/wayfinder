package net.jerryjeanbaptiste.wayfinder.ui;

// Bryce Schweitzer 2015

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.jerryjeanbaptiste.wayfinder.R;
import net.jerryjeanbaptiste.wayfinder.objects.RealmBuilding;
import net.jerryjeanbaptiste.wayfinder.objects.RealmDepartment;

import java.util.Timer;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class DepartmentActivity extends ActionBarActivity {

    RealmDepartment dbDepartment;
    String buildingName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_department);

        Intent intent = getIntent();
        final String intent_name= intent.getStringExtra("name");

        //Initialize Views
        //ImageView Banner
        ImageView deptBannerImage = (ImageView) findViewById(R.id.deptHeaderImageView);
        //TextViews
        TextView departmentName = (TextView) findViewById(R.id.deptNameTextView);
        TextView departmentDesc = (TextView) findViewById(R.id.deptDescriptionTextView);
        TextView departmentPhone = (TextView) findViewById(R.id.deptPhoneNumber);
        Button button = (Button) findViewById(R.id.DepartmentGo);

        if(intent_name!=null)
        {
            Realm realm = Realm.getInstance(this);
            RealmQuery<RealmDepartment> query = realm.where(RealmDepartment.class);
            query.equalTo("DepartmentName",intent_name);
            RealmResults<RealmDepartment> results= query.findAll();
            dbDepartment= results.first();

            //Set Building Text
            departmentName.setText(dbDepartment.getDepartmentName());
            departmentDesc.setText(dbDepartment.getDepartmentDescription());
            departmentPhone.setText(dbDepartment.getDepartmentPhone());
            buildingName= dbDepartment.getBuildingName();
            Toast.makeText(this,buildingName,Toast.LENGTH_LONG).show();
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(DepartmentActivity.this,MapActivity.class);
                    intent.putExtra("map_type",MapActivity.LOCATION_TO_BUILDING);
                    intent.putExtra("building",buildingName);
                    startActivity(intent);
                }
            });

        }
        else
        {

            //Set Building Text
            departmentName.setText("Computer Programming Department");
            departmentDesc.setText("This is a description for the Computer Programming Department.");
        }






        //Set Banner
        deptBannerImage.setImageResource(R.drawable.csbanner);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_building, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
