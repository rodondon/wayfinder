package net.jerryjeanbaptiste.wayfinder.ui;

//Bryce Schweitzer 2015

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.jerryjeanbaptiste.wayfinder.R;

public class AdminHomepageActivity extends ActionBarActivity {

    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = getSharedPreferences("net.jerryjeanbaptiste.wayfinder", MODE_PRIVATE);
        if(prefs.getBoolean("AdminLoggedIn",false)) {
            setContentView(R.layout.activity_admin_homepage);

            //Initialize Views
            Button editBuildingButton = (Button) findViewById(R.id.editBuildingButton);
            Button editPathwayButton = (Button) findViewById(R.id.editPathwayButton);
            Button getReportButton = (Button) findViewById(R.id.getReportButton);

            editBuildingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AdminHomepageActivity.this, EditBuildingActivity.class);
                    startActivity(intent);
                }
            });
            editPathwayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AdminHomepageActivity.this, EditPathwayActivity.class);
                    startActivity(intent);
                }
            });
            getReportButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AdminHomepageActivity.this, ReportActivity.class);
                    startActivity(intent);
                }
            });
        }
        else
        {
            Toast.makeText(this,"You need to loggin first", Toast.LENGTH_LONG).show();
            Intent intent = new  Intent (this,LoginActivity.class);
            startActivity(intent);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_building, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==R.id.admin_logout)
        {
            prefs.edit().putBoolean("UserLoggedIn",false).putBoolean("AdminLoggedIn",false).commit();
            Intent intent = new Intent(this,HomeActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

}
