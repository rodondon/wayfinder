package net.jerryjeanbaptiste.wayfinder.ui;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;


import com.software.shell.fab.ActionButton;

import net.jerryjeanbaptiste.wayfinder.R;
import net.jerryjeanbaptiste.wayfinder.objects.AlarmReceiver;
import net.jerryjeanbaptiste.wayfinder.objects.RealmBuilding;
import net.jerryjeanbaptiste.wayfinder.objects.RealmCourse;
import net.jerryjeanbaptiste.wayfinder.objects.RealmSchedule;
import net.jerryjeanbaptiste.wayfinder.objects.RealmScheduleDay;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class ScheduleActivity extends ActionBarActivity {

    Spinner days;
    ListView courses;
    RealmSchedule schedule;
    SharedPreferences prefs;
    Spinner buildingsSpinner;
    Realm realm;
    ActionButton actionButton;
    public static String[] dayNames = {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
    public static int ADD_NEW_COURSE=0;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_NEW_COURSE) {
            if(resultCode== Activity.RESULT_OK)
            {
                buildSchedule(days.getSelectedItemPosition());
                setAlarms();
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        prefs = getSharedPreferences("net.jerryjeanbaptiste.wayfinder", MODE_PRIVATE);
        if(prefs.getBoolean("UserLoggedIn",true)){
            courses = (ListView) findViewById(R.id.CoursesListView);
            actionButton = (ActionButton) findViewById(R.id.action_button);
            actionButton.setImageDrawable(getResources().getDrawable(R.drawable.fab_plus_icon));
            actionButton.setButtonColor(getResources().getColor(R.color.ActionBar));
            actionButton.setButtonColorPressed(getResources().getColor(R.color.DarkActionBar));

            realm = Realm.getInstance(this);


            initialize();


        }
        else
        {
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
        }

    }

    private void initialize()
    {
        loadDays();
        listenForDays();
        getScheduleObject();
        buildSchedule(0);
        listenForNewCourse();


    }
    public void setAlarms()
    {

        RealmSchedule schedule = realm.where(RealmSchedule.class).findFirst();
        RealmList<RealmScheduleDay> scheduleDays= schedule.getDaysSchedules();

        AlarmManager alarmMgr = (AlarmManager) this.getSystemService(this.ALARM_SERVICE);
        for(int i=0;i<scheduleDays.size();i++) {


        for(int b=0;b<scheduleDays.get(i).getCourses().size();b++) {
            RealmCourse course = scheduleDays.get(i).getCourses().get(b);
            Intent intent = new Intent(this, AlarmReceiver.class);
            intent.putExtra("course", course.getName());
            intent.putExtra("buildingName",course.getBuildingName());
            //sendBroadcast(intent);
            PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 454545422 + i+b, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            // Set the alarm to start at 8:30 a.m.
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());

            calendar.set(Calendar.DAY_OF_WEEK, 2);
            int hour =course.getStartHour();

            if(course.getPeriod_of_day().equals("PM") && course.getStartHour()!=12)
                hour=course.getStartHour()+12;
            Toast.makeText(this,"hour "+hour, Toast.LENGTH_LONG).show();
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, course.getStartMinute());
            calendar.add(Calendar.MINUTE,-10);

            // setRepeating() lets you specify a precise custom interval--in this case,
            // 20 minutes.
            alarmMgr.set(AlarmManager.RTC, calendar.getTimeInMillis(), alarmIntent);
        }
        }

    }
    private void listenForNewCourse()
    {
            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ScheduleActivity.this,AddCourseActivity.class);
                    intent.putExtra("Day",days.getSelectedItemPosition());
                    startActivityForResult(intent, 0);
                }
            });
//        final EditText new_course = (EditText) findViewById(R.id.NewCourseEditTExt);
//        final EditText statTime = (EditText) findViewById(R.id.StartHourEditText);
//        Button add_course = (Button) findViewById(R.id.addCourseButton);
//        add_course.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                realm.beginTransaction();
//                RealmResults<RealmScheduleDay> find = realm.where(RealmScheduleDay.class).equalTo("name",dayNames[days.getSelectedItemPosition()]).findAll();
//                RealmScheduleDay day=find.first();
//                RealmCourse realmCourse=realm.createObject(RealmCourse.class);
//                realmCourse.setName(new_course.getText().toString());
//                realmCourse.setStart(statTime.getText().toString());
//                day.getCourses().add(realmCourse);
//                realm.commitTransaction();
//                buildSchedule(days.getSelectedItemPosition());
//            }
//        });
    }

    private void getScheduleObject()
    {

        if (prefs.getBoolean("firstScheduleRun", true)) {

            realm.beginTransaction();

            RealmSchedule schedule1= realm.createObject(RealmSchedule.class);
            RealmList<RealmScheduleDay> realmScheduleDays= new RealmList<>();

            for(int i=0;i<7;i++)
            {
                RealmScheduleDay realmScheduleDay= realm.createObject(RealmScheduleDay.class);
                realmScheduleDay.setName(dayNames[i]);
                //realmScheduleDay.setCourses(new RealmList<RealmCourse>());
                realmScheduleDays.add(realmScheduleDay);
            }
            schedule1.setDaysSchedules(realmScheduleDays);

            realm.commitTransaction();

            prefs.edit().putBoolean("firstScheduleRun", false).commit();
        }

        RealmQuery<RealmSchedule> query = realm.where(RealmSchedule.class);
        RealmResults<RealmSchedule> results =query.findAll();
        schedule=results.first();
    }
    private void buildSchedule(int day)
    {

        RealmList<RealmScheduleDay> daysSchedules=schedule.getDaysSchedules();
        RealmList<RealmCourse> courseRealmList= daysSchedules.get(day).getCourses();

        ArrayList<String> coursesArrayList= new ArrayList<String>();
        for(RealmCourse crs : courseRealmList)
        {
            coursesArrayList.add(crs.getStartHour()+" : "+crs.getStartMinute()+" "+crs.getPeriod_of_day()+" - "+crs.getName());
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,coursesArrayList);

        courses.setAdapter(adapter);
    }

    private  void loadDays()
    {
        days = (Spinner) findViewById(R.id.DaySpinner);

        ArrayAdapter<String> spinnerAdapter= new ArrayAdapter<String>(this,R.layout.farmingdale_spinner_item,dayNames);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        days.setAdapter(spinnerAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!prefs.getBoolean("UserLoggedIn",false))
        {
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
        }
    }
    private void listenForDays()
    {
         days.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    buildSchedule(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            }
         );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_schedule, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==R.id.logout)
        {
            prefs.edit().putBoolean("UserLoggedIn",false).putBoolean("AdminLoggedIn",false).commit();
            Intent intent = new Intent(this,HomeActivity.class);
            startActivity(intent);
        }
        if(id==R.id.delete_schedule)
        {
            realm.beginTransaction();
            RealmResults<RealmSchedule> realmSchedule = realm.where(RealmSchedule.class).findAll();
            RealmResults<RealmScheduleDay>realmScheduleDay= realm.where(RealmScheduleDay.class).findAll();
            RealmResults<RealmCourse> realmCourses=realm.where(RealmCourse.class).findAll();
            realmSchedule.clear();
            realmScheduleDay.clear();
            realmCourses.clear();
            realm.commitTransaction();
            Toast.makeText(this,"Schedule deleted",Toast.LENGTH_LONG).show();
            prefs.edit().putBoolean("firstScheduleRun", true).commit();
            initialize();
        }

        return super.onOptionsItemSelected(item);
    }
}
