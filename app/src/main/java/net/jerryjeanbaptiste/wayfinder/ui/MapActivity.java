package net.jerryjeanbaptiste.wayfinder.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import net.jerryjeanbaptiste.wayfinder.R;
import net.jerryjeanbaptiste.wayfinder.objects.BitmapWorkerTask;
import net.jerryjeanbaptiste.wayfinder.objects.Graph;
import net.jerryjeanbaptiste.wayfinder.objects.Point;
import net.jerryjeanbaptiste.wayfinder.objects.RealmVertex;
import net.jerryjeanbaptiste.wayfinder.objects.TouchImageView;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 *
 */
public class MapActivity extends ActionBarActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    Location mLastLocation;
    Point userLocation;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Realm realm;
    TouchImageView map;
    int map_type;
    RealmVertex closest_point;

    public final static int SHOW_REAL_POINT= 0;
    public final static int SHOW_REAL_PATH=2;
    public final static int BUILDING_TO_BUILDING=1;
    public final static int LOCATION_TO_BUILDING=3;
    private Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intent = getIntent();
        realm = Realm.getInstance(this);

        setContentView(R.layout.activity_map);
        map = (TouchImageView) findViewById(R.id.mapImageView);



        buildGoogleApiClient();
        mGoogleApiClient.connect();
        createLocationRequest();
        Map();


    }
    private void Map()
    {
        BitmapWorkerTask task = new BitmapWorkerTask(map,this);
        if(intent.getExtras()!=null) {

            map_type = intent.getExtras().getInt("map_type");
            if (map_type == SHOW_REAL_POINT)
            {
                double point_lat=intent.getExtras().getDouble("latitude");
                double point_lon=intent.getExtras().getDouble("longitude");
                Point realPoint = getMapPoint(point_lat,point_lon);

                task.execute(R.drawable.map,map_type,realPoint);
            }
            else if(map_type==SHOW_REAL_PATH)
            {
                double point_lat=intent.getExtras().getDouble("latitude");
                double point_lon=intent.getExtras().getDouble("longitude");
                String buildingName = intent.getExtras().getString("building");

                Point userLocation = getMapPoint(point_lat, point_lon);

                Graph graph = new Graph(realm);
                ArrayList<Point> points = graph.getDrawablePath(closest_point.getName(),buildingName);

                task.execute(R.drawable.map,map_type,points);

            }
            else if (map_type==BUILDING_TO_BUILDING)
            {
                String buildingName1 = intent.getExtras().getString("building1");
                String buildingName2 = intent.getExtras().getString("building2");
                Graph graph = new Graph(realm);
                ArrayList<Point> points = graph.getDrawablePath(buildingName1,buildingName2);
                task.execute(R.drawable.map,SHOW_REAL_PATH,points);
            }
            else if(map_type==LOCATION_TO_BUILDING)
            {
                String building = intent.getExtras().getString("building");
                if(mLastLocation==null)
                    getMapPoint(40.752429,-73.427911);
                else
                    getMapPoint(mLastLocation.getLatitude(),mLastLocation.getLongitude());

                if(building==null)
                    building="Hale Hall";
                Graph graph = new Graph(realm);
                ArrayList<Point> points = graph.getDrawablePath(closest_point.getName(),building);
                task.execute(R.drawable.map,SHOW_REAL_PATH,points);
            }


        }
        else
            task.execute(R.drawable.map);
    }
    private Point getMapPoint(double point_lat,double point_long)
    {
        RealmResults<RealmVertex> results = realm.where(RealmVertex.class).findAll();
        double smallest_dist;
        closest_point=results.first();
        smallest_dist = distance(new Point(Double.parseDouble(results.first().getLatitude()),Double.parseDouble(results.first().getLongitude())),
                new Point(point_lat,point_long));

        for(RealmVertex vertex : results)
        {
            double current_lat=Double.parseDouble(vertex.getLatitude());
            double current_long=Double.parseDouble(vertex.getLongitude());
            double current_dist=distance(new Point(current_lat,current_long), new Point(point_lat,point_long));

            if(current_dist<smallest_dist)
            {
                closest_point=vertex;
                smallest_dist=current_dist;

            }
        }
        //1335*1887
        //"x":2044,"y":580
        //Campus center "x":1472,"y":577
        return toSmallMap(new Point(closest_point.getX(),closest_point.getY()));


    }




    public static Point toSmallMap(Point point)
    {
        double y_percent=point.getY()*100/1778;
        double x_percent=point.getX()*100/2513;

        double map_y= y_percent*1335/100;
        double map_x=x_percent*1887/100;
        return new Point(map_x,map_y);
    }
    public Point smallToLarge(Point small)
    {
        Point large = new Point();
        large.setX((small.getX() * 100 / 1887) * 2513 / 100);
        large.setY((small.getY()*100/1335)*1778/100);
        return  large;
    }
    public static double distance(Point A,Point B)
    {
        //double dist =  Math.sqrt(Math.pow(A.getX() - B.getY(), 2) + Math.pow(A.getY() - B.getY(), 2) );
        float[] result= new float[1];
        Location.distanceBetween(A.getY(),A.getX(),B.getY(),B.getX(),result);
        return result[0];
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        if (mGoogleApiClient.isConnected())
            stopLocationUpdates();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }

    }
    protected void startLocationUpdates() {
        try
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,MapActivity.this);
        }
        catch (Exception $e)
        {
            Log.e("MainActivity-LOG", $e.getMessage() + "\n" + $e.toString());
        }
    }
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            userLocation=getPoint(mLastLocation.getLatitude(),mLastLocation.getLongitude());
            Toast.makeText(MapActivity.this,mLastLocation.getLatitude()+ " "+ mLastLocation.getLongitude(),Toast.LENGTH_LONG).show();
        }
        startLocationUpdates();


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation=new Location(location);
        //Toast.makeText(this,location.getLatitude()+" "+location.getLongitude(),Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(this.getClass().getCanonicalName(),"cannot get Location");

    }

    public static Point getPoint(double lat, double lon) {
        Point point = new Point();
        point.setX(lon);
        point.setY(lat);
        point.rotate(30);
        double x = (1887*(point.getX()*100)/0.00067)/100;
        double y = (1335*(point.getY()*100)/0.00045)/100;
        //double x = (180+point.getX()) * (1887 / 360);
        //double y = (90-point.getY()) * (1335 / 180);
        point.setX(x);
        point.setY(1335-y);

        return point;
    }
}
