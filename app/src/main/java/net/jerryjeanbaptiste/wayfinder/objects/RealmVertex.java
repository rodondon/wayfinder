package net.jerryjeanbaptiste.wayfinder.objects;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Jerry on 5/10/2015.
 */
public class RealmVertex extends RealmObject {
    private String name;
    private String latitude;
    private String longitude;
    private String isBuilding;
    private int x;
    private int y;
    private RealmList<RealmAdjacency> adjacencies;

    public RealmList<RealmAdjacency> getAdjacencies() {
        return adjacencies;
    }

    public void setAdjacencies(RealmList<RealmAdjacency> adjacencies) {
        this.adjacencies = adjacencies;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String isBuilding() {
        return isBuilding;
    }

    public void setIsBuilding(String isBuilding) {
        this.isBuilding = isBuilding;
    }
}
