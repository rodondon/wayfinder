package net.jerryjeanbaptiste.wayfinder.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.jerryjeanbaptiste.wayfinder.R;
import net.jerryjeanbaptiste.wayfinder.objects.RealmUser;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class LoginActivity extends ActionBarActivity {

    EditText username,password;
    Button submit;
    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        prefs = getSharedPreferences("net.jerryjeanbaptiste.wayfinder", MODE_PRIVATE);
        if(prefs.getBoolean("AdminLoggedIn",false))
        {
            Intent intent = new Intent(LoginActivity.this,AdminHomepageActivity.class);
            startActivity(intent);
        }
        else if(prefs.getBoolean("UserLoggedIn",false))
        {
            Intent intent = new Intent(LoginActivity.this,ScheduleActivity.class);
            startActivity(intent);
        }
        loadViews();
        loadListeners();
    }

    private void loadListeners() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username_text = username.getText().toString();
                String password_text= password.getText().toString();
                if(username_text.equals("Admin") && password_text.equals("Admin"))
                {
                    prefs.edit().putBoolean("AdminLoggedIn", true).commit();
                    prefs.edit().putBoolean("UserLoggedIn", true).commit();
                    Intent intent = new Intent(LoginActivity.this,AdminHomepageActivity.class);
                    startActivity(intent);
                    finish();
                }
                Realm realm = Realm.getInstance(LoginActivity.this);
                RealmQuery<RealmUser> query= realm.where(RealmUser.class);
                RealmResults<RealmUser> results = query.equalTo("username",username_text).equalTo("password", Register.sha256(password_text, LoginActivity.this)).findAll();
                if(results.size()>0)
                {
                    prefs.edit().putBoolean("UserLoggedIn", true).commit();
                    Toast.makeText(LoginActivity.this,"Login Successful",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(LoginActivity.this,ScheduleActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    prefs.edit().putBoolean("UserLoggedIn", false).commit();
                    Toast.makeText(LoginActivity.this,"Wrong username and/or password",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void loadViews() {
        username = (EditText) findViewById(R.id.loginUsernameEditText);
        password = (EditText) findViewById(R.id.loginPasswordEditText);
        submit = (Button) findViewById(R.id.loginSubmit);
    }
    @Override
    protected void onResume() {
        super.onResume();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==R.id.Register_page){
            Intent intent = new Intent(this, Register.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
