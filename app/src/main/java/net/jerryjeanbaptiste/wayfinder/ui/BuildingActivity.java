package net.jerryjeanbaptiste.wayfinder.ui;

//Bryce Schweitzer 2015

import android.content.Intent;
import android.database.DataSetObserver;
import android.media.Image;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import net.jerryjeanbaptiste.wayfinder.R;
import net.jerryjeanbaptiste.wayfinder.objects.RealmBuilding;
import net.jerryjeanbaptiste.wayfinder.objects.RealmDepartment;

import java.util.ArrayList;
import java.util.Collections;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class BuildingActivity extends ActionBarActivity {

    RealmBuilding dbBuilding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building);

        ImageView buildingImg = (ImageView) findViewById(R.id.buildingImageView);

        //TextView
        TextView buildingName = (TextView) findViewById(R.id.buildingNameTextView);
        TextView buildingDesc = (TextView) findViewById(R.id.buildingDescriptionTextView);
        ListView buildingDepartmentLV = (ListView) findViewById(R.id.buildingDepartmentListView);


        Intent intent = getIntent();
        final String intent_name= intent.getStringExtra("name");
        final ArrayList<String> depList= new ArrayList<>();
        if(intent_name!=null)
        {
            Realm realm = Realm.getInstance(this);
            RealmQuery<RealmBuilding> query = realm.where(RealmBuilding.class);
            query.equalTo("BuildingName",intent_name);
            RealmResults<RealmBuilding> results= query.findAll();
            dbBuilding= results.first();
            //Set Building Text
            buildingName.setText(dbBuilding.getBuildingName());
            buildingDesc.setText(dbBuilding.getBuildingDescription());
            RealmList<RealmDepartment> departmentRealmList= dbBuilding.getDepartments();

            for(RealmDepartment dep: departmentRealmList )
            {
                depList.add(dep.getDepartmentName());
            }
            Collections.sort(depList);
        }
        else
        {
            buildingName.setText("Building Name");
            buildingDesc.setText("Building Description");

        }
        ArrayAdapter<String> deptListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,depList);
        //Initialize Views
        //ImageView


        //Set Image
        buildingImg.setImageResource(R.drawable.university);



        //Adapter For Department List
        buildingDepartmentLV.setAdapter(deptListAdapter);
//        deptListAdapter.add("Dept1");
//        deptListAdapter.add("Dept2");
//        deptListAdapter.add("Dept3");

        //Listener For Department List Click
        buildingDepartmentLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent startDeptActivityIntent = new Intent(BuildingActivity.this, DepartmentActivity.class);
                if(intent_name!=null)
                {
                    startDeptActivityIntent.putExtra("name",depList.get(position));
                }

                //Find which building the dept list was generated for
                //Identify department based on building and position of click on list


                /*
                Switch or If to determine which department...
                Set identified department based on results...
                String deptName = "";

                //Pass correct department to display in DepartmentActivity in intent
                startDeptActivityIntent.putExtra("Passed Department", deptName);
                */

                //Start DepartmentActivity
                startActivity(startDeptActivityIntent);

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_building, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
