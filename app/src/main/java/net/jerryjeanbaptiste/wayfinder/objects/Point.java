package net.jerryjeanbaptiste.wayfinder.objects;

/**
 * Created by Jerry on 5/4/2015.
 */
public class Point {
    double  x;
    double y;

    public  Point(double x, double y)
    {
        setX(x);
        setY(y);
    }
    public Point()
    {

    }
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    //clockwise rotation
    public void rotate (double angle)
    {
        angle=Math.toRadians(angle);
        double temp_x= x;
        x=(Math.cos(angle)*x)+(Math.sin(angle)*y*(-1));
        y=(Math.sin(angle)*temp_x)+(Math.cos(angle)*y);
    }
}
