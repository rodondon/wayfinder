package net.jerryjeanbaptiste.wayfinder.objects;

import io.realm.RealmObject;

/**
 * Created by Jerry on 2/14/2015.
 * This is a link between two nodes
 */
public class Edge
{
    public final Vertex target;
    public final double weight;
    public String name;
    public Edge(Vertex argTarget, double argWeight)
    { target = argTarget; weight = argWeight; }
    public Edge(Vertex argTarget, double argWeight,String name)
    { target = argTarget; weight = argWeight; this.name=name; }

    public Vertex getTarget() {
        return target;
    }

    public double getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}