package net.jerryjeanbaptiste.wayfinder.objects;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Jerry on 4/19/2015.
 */
public class RealmSchedule extends RealmObject {
    private RealmList<RealmScheduleDay> DaysSchedules;


    public RealmList<RealmScheduleDay> getDaysSchedules() {
        return DaysSchedules;
    }

    public void setDaysSchedules(RealmList<RealmScheduleDay> daysSchedules) {
        DaysSchedules = daysSchedules;
    }
}
