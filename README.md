# Farmingdale WayFinder#



### What is this repository for? ###

* This repository houses the source code of the WayFinder Android application. The goal of this application is to help students navigate the campus quickly.
* Version 0.9 (Beta)

### Screenshots ###
![rsz_1rsz_2015-06-17_145635.png](https://bitbucket.org/repo/nARGdR/images/70266210-rsz_1rsz_2015-06-17_145635.png)
![rsz_2015-06-17_150041.png](https://bitbucket.org/repo/nARGdR/images/2228603125-rsz_2015-06-17_150041.png)
### How do I get set up? ###

* Install Android Studio
* Install git 
* clone the repository [How ?](https://confluence.atlassian.com/display/BITBUCKET/Clone+a+repository#Clonearepository-CloningaGitrepository)

 
### Contribution guidelines ###

*  message admin


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


### Sample Graph ###
![Untitled drawing.png](https://bitbucket.org/repo/nARGdR/images/458035678-Untitled%20drawing.png)