package net.jerryjeanbaptiste.wayfinder.ui;

//Bryce Schweitzer 2015

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import net.jerryjeanbaptiste.wayfinder.R;

public class EditPathwayActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pathway);

        TextView endpoint1TV = (TextView)findViewById(R.id.endpoint1TextView);
        EditText endpoint1ET = (EditText)findViewById(R.id.endpoint1EditText);
        TextView endpoint2TV = (TextView)findViewById(R.id.endpoint2TextView);
        EditText endpoint2ET = (EditText)findViewById(R.id.endpoint2EditText);


        TextView selectPathwayHeaderTV = (TextView) findViewById(R.id.selectPathwayTextView);
        ListView pathwayLV = (ListView) findViewById(R.id.pathwayListView);
        ArrayAdapter<String> pathwayListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

        pathwayLV.setAdapter(pathwayListAdapter);
        pathwayListAdapter.add("Pathway 1");
        pathwayListAdapter.add("Pathway 2");
        pathwayListAdapter.add("Pathway 3");

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_building, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
