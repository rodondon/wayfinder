package net.jerryjeanbaptiste.wayfinder.objects;

/**
 * Created by Jerry on 2/14/2015.
 * This is the representation of a segment between two vertices
 */
public class WalkWay extends Edge {

    public WalkWay(Vertex argTarget, double argWeight) {
        super(argTarget, argWeight);
    }
    public WalkWay(Vertex argTarget,double argWeight, String name)
    {
        super(argTarget,argWeight,name);
    }
}
