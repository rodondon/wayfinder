package net.jerryjeanbaptiste.wayfinder.ui;

//Bryce Schweitzer 2015

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import net.jerryjeanbaptiste.wayfinder.R;

public class AdminLoginActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        //Initialize Views
        TextView adminLoginHeaderTextView = (TextView) findViewById(R.id.adminLoginHeaderTextView);
        TextView adminUsernameTextView = (TextView) findViewById(R.id.adminUsernameTextView);
        EditText adminUsernameEditText = (EditText) findViewById(R.id.adminUsernameEditText);
        TextView adminPasswordTextView = (TextView) findViewById(R.id.adminPasswordTextView);
        EditText adminPasswordEditText = (EditText) findViewById(R.id.adminPasswordEditText);
        Button adminLoginButton = (Button) findViewById(R.id.adminLoginButton);

        adminLoginHeaderTextView.setText("Login As An Administrator");
        adminUsernameTextView.setText("Admin Username");
        adminPasswordTextView.setText("Admin Password");

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_building, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
