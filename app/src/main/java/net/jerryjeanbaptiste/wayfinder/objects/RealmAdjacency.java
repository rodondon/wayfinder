package net.jerryjeanbaptiste.wayfinder.objects;

import io.realm.RealmObject;

/**
 * Created by Jerry on 5/10/2015.
 */
public class RealmAdjacency extends RealmObject {

    private String target;
    private String name;
    private double weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
