package net.jerryjeanbaptiste.wayfinder.objects;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by jerry1 on 5/9/15.
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context,"Alarm received",Toast.LENGTH_LONG).show();
        Intent service1 = new Intent(context, NotificationService.class);
        if(intent.getExtras()!=null)
        service1.putExtra("course",intent.getExtras().getString("course"));
        context.startService(service1);
    }
}
