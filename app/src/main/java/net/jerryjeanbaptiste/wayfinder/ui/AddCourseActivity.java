package net.jerryjeanbaptiste.wayfinder.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import net.jerryjeanbaptiste.wayfinder.R;
import net.jerryjeanbaptiste.wayfinder.objects.RealmBuilding;
import net.jerryjeanbaptiste.wayfinder.objects.RealmCourse;
import net.jerryjeanbaptiste.wayfinder.objects.RealmScheduleDay;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class AddCourseActivity extends ActionBarActivity {

    int editing_day;
    Realm realm;
    Button submit;
    EditText new_course,startHour,startMinute;
    Spinner buildingsSpinner,amPmSpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_course);
        Intent intent = getIntent();
        editing_day=intent.getExtras().getInt("Day");
        realm = Realm.getInstance(this);

        loadViews();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(startHour.getText().length()>0 && startMinute.getText().length()>0 && new_course.getText().length()>0) {
                    RealmBuilding selectedBuilding= realm.where(RealmBuilding.class).equalTo("BuildingName",buildingsSpinner.getSelectedItem().toString()).findFirst();
                    realm.beginTransaction();
                    RealmResults<RealmScheduleDay> find = realm.where(RealmScheduleDay.class).equalTo("name", ScheduleActivity.dayNames[editing_day]).findAll();
                    RealmScheduleDay day = find.first();
                    RealmCourse realmCourse = realm.createObject(RealmCourse.class);
                    realmCourse.setName(new_course.getText().toString());
                    realmCourse.setStartHour(Integer.parseInt(startHour.getText().toString()));
                    realmCourse.setStartMinute(Integer.parseInt(startMinute.getText().toString()));
                    realmCourse.setPeriod_of_day(amPmSpinner.getSelectedItem().toString());
                    realmCourse.setBuildingName(selectedBuilding.getBuildingName());
                    day.getCourses().add(realmCourse);
                    realm.commitTransaction();
                    //buildSchedule(days.getSelectedItemPosition());
                    Intent result = new Intent("com.example.RESULT_ACTION", Uri.parse("content://result_uri"));
                    setResult(Activity.RESULT_OK, result);
                    finish();
                }
            }
        });
    }
    private void loadViews()
    {
        submit = (Button) findViewById(R.id.AddCourseButton);
        new_course= (EditText) findViewById(R.id.NewCourseEditTExt);
        startHour = (EditText) findViewById(R.id.StartHourEditText);
        startMinute =(EditText) findViewById(R.id.startMinuteEditText);
        buildingsSpinner = (Spinner) findViewById(R.id.addCourseBuildingSpinner);
        amPmSpinner = (Spinner) findViewById(R.id.amPmSpinner);


        RealmQuery<RealmBuilding> buildingRealmQuery = realm.where(RealmBuilding.class);
        RealmResults<RealmBuilding> buildingRealmResults= buildingRealmQuery.findAll();
        buildingRealmResults.sort("BuildingName");
        ArrayAdapter<String> buildingListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        buildingListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        for(RealmBuilding item : buildingRealmResults)
        {
            buildingListAdapter.add(item.getBuildingName());
        }
        buildingsSpinner.setAdapter(buildingListAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_course, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
