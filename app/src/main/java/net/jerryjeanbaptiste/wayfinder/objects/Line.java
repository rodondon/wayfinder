package net.jerryjeanbaptiste.wayfinder.objects;

/**
 * Created by Jerry on 5/4/2015.
 */
public class Line {
    private Point a;
    private Point b;

    Line(Point start, Point end)
    {
        a=start;
        b=end;

    }
    Line()
    {

    }

    public Point getA() {
        return a;
    }

    public void setA(Point a) {
        this.a = a;
    }

    public Point getB() {
        return b;
    }

    public void setB(Point b) {
        this.b = b;
    }
}
