package net.jerryjeanbaptiste.wayfinder.objects;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import net.jerryjeanbaptiste.wayfinder.R;
import net.jerryjeanbaptiste.wayfinder.ui.MainActivity;
import net.jerryjeanbaptiste.wayfinder.ui.MapActivity;
import net.jerryjeanbaptiste.wayfinder.ui.ScheduleActivity;

/**
 * Created by Jerry on 5/10/2015.
 */
public class NotificationService extends IntentService {

    /**
     * A constructor is required, and must call the super IntentService(String)
     * constructor with a name for the worker thread.
     */
    public NotificationService() {
        super("TestService");
    }

    /**
     * The IntentService calls this method from the default worker thread with
     * the intent that started the service. When this method returns, IntentService
     * stops the service, as appropriate.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        // Normally we would do some work here, like download a file.
        String name =intent.getExtras().getString("buildingName");
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Don't be late for "+intent.getExtras().getString("course"))
                        .setContentText("Your class will begin in 10 mins.");
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this,MapActivity.class);
        resultIntent.putExtra("map_type",MapActivity.LOCATION_TO_BUILDING);
        resultIntent.putExtra("building","Hale Hall");

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(ScheduleActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(0, mBuilder.build());
    }
}