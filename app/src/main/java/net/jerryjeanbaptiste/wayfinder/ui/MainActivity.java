package net.jerryjeanbaptiste.wayfinder.ui;


import android.content.Intent;
import android.location.Location;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import net.jerryjeanbaptiste.wayfinder.R;
import net.jerryjeanbaptiste.wayfinder.objects.Building;
import net.jerryjeanbaptiste.wayfinder.objects.Dijkstra;
import net.jerryjeanbaptiste.wayfinder.objects.Edge;
import net.jerryjeanbaptiste.wayfinder.objects.Intersection;
import net.jerryjeanbaptiste.wayfinder.objects.RealmAdjacency;
import net.jerryjeanbaptiste.wayfinder.objects.RealmBuilding;
import net.jerryjeanbaptiste.wayfinder.objects.RealmVertex;
import net.jerryjeanbaptiste.wayfinder.objects.Vertex;
import net.jerryjeanbaptiste.wayfinder.objects.WalkWay;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class MainActivity extends ActionBarActivity implements
       GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    ArrayList<Building> buildings;
    HashMap<String, Vertex> vertexes;
    Spinner origin;
    Spinner destination;
    TextView results, mLongitudeText,mLatitudeText;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Realm realm;
    ListView route;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        realm = Realm.getInstance(this);
        buildGraph();
        populateBuildings();
        listenForPath();

        Button mapView = (Button) findViewById(R.id.mapView);
        //mLatitudeText = (TextView) findViewById(R.id.Latitude);
        //mLongitudeText = (TextView) findViewById(R.id.Longitude);


        buildGoogleApiClient();
        mGoogleApiClient.connect();
        createLocationRequest();
        mapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MapActivity.class);
                String building1 = origin.getSelectedItem().toString();
                String building2 = destination.getSelectedItem().toString();
                intent.putExtra("map_type", MapActivity.BUILDING_TO_BUILDING);
//                intent.putExtra("latitude",40.751969);
//                intent.putExtra("longitude",-73.429287);
                intent.putExtra("building1",building1);
                intent.putExtra("building2",building2);
                startActivity(intent);

            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected())
            stopLocationUpdates();
    }
    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    /**
     * This will fill the spinners with lists of all the available buildings
     * TODO: Load building list from database
     */
    public void populateBuildings()
    {
        RealmQuery<RealmBuilding> buildingRealmQuery = realm.where(RealmBuilding.class);
        RealmResults<RealmBuilding> buildingRealmResults= buildingRealmQuery.findAll();
        buildingRealmResults.sort("BuildingName");
        ArrayAdapter<String> buildingListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        buildingListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        for(RealmBuilding item : buildingRealmResults)
        {
            buildingListAdapter.add(item.getBuildingName());
        }
        origin = (Spinner) findViewById(R.id.originSpinner);
        destination = (Spinner) findViewById(R.id.destinationSpinner);

        origin.setAdapter(buildingListAdapter);
        destination.setAdapter(buildingListAdapter);
//        ArrayList<String> buildings_names = new ArrayList<>();
//        for(int i=0;i<buildings.size();i++)
//        {
//            buildings_names.add(buildings.get(i).name);
//        }


//        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,buildings_names);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        origin.setAdapter(adapter);
//        destination.setAdapter(adapter);
    }
    public void buildGraph()
    {
        vertexes = new HashMap<String,Vertex>();
        RealmResults<RealmVertex> list  = realm.where(RealmVertex.class).findAll();
        for(RealmVertex vertex: list)
        {
            if(vertex.isBuilding().equals("true"))
                vertexes.put(vertex.getName(), new Building(vertex));
            else
                vertexes.put(vertex.getName(), new Intersection(vertex));
        }
        for(int i=0;i<vertexes.size();i++)
        {
            RealmList<RealmAdjacency> currentRealmList=list.get(i).getAdjacencies();
            Edge[] adjacencies = new Edge[currentRealmList.size()];
            for(int b=0;b<currentRealmList.size();b++)
            {
                if(vertexes.containsKey(currentRealmList.get(b).getTarget()) && vertexes.get(currentRealmList.get(b).getTarget())!=null )
                    adjacencies[b]=new WalkWay(vertexes.get(currentRealmList.get(b).getTarget()),currentRealmList.get(b).getWeight(),currentRealmList.get(b).getName());
            }
            vertexes.get(list.get(i).getName()).setAdjacencies(adjacencies);
        }
//        Building hale = new Building("Hale Hall");
//        Intersection a1_b1= new Intersection("a1_b1");
//        Intersection b1_c1 = new Intersection("b1_c1");
//        Intersection d1_e1 = new Intersection("d1_e1");
//        Intersection e1_f1_g1 = new Intersection("e1_f1_g1");
//
//        Building greenley = new Building("Greenley Library");
//        Intersection g1_l1 = new Intersection("g1_l1");
//        Intersection i1_h1_j1 = new Intersection("i1_h1_j1");
//
//        Building lupton = new Building("Lupton Hall");
//        Building roosevelt = new Building("Roosevelt Hall");
//
//        buildings = new ArrayList<>();
//        buildings.add(hale);
//        buildings.add(greenley);
//        buildings.add(lupton);
//        buildings.add(roosevelt);
//
//        hale.adjacencies= new WalkWay[]{
//          new WalkWay(a1_b1,169.84,"A1"),
//          new WalkWay(d1_e1,133.73,"D1"),
//          new WalkWay(lupton,317.86,"K1")
//        };
//        a1_b1.adjacencies=new WalkWay[]{
//            new WalkWay(b1_c1,97.6,"B1"),
//            new WalkWay(hale,169.84,"A1")
//        };
//        b1_c1.adjacencies= new WalkWay[]{
//                new WalkWay(greenley,74.44,"C1"),
//                new WalkWay(a1_b1,97.6,"B1")
//        };
//        d1_e1.adjacencies= new WalkWay[]{
//                new WalkWay(e1_f1_g1,110.71,"E1"),
//                new WalkWay(hale,133.73,"D1")
//        };
//        e1_f1_g1.adjacencies= new WalkWay[]{
//                new WalkWay(greenley,84.7,"F1"),
//                new WalkWay(g1_l1,249.08,"G1"),
//                new WalkWay(d1_e1,110.71,"E1")
//        };
//        g1_l1.adjacencies=new WalkWay[] {
//            new WalkWay(roosevelt,56.61,"L1"),
//            new WalkWay(e1_f1_g1,249.08,"G1")
//        };
//        roosevelt.adjacencies = new WalkWay[]{
//                new WalkWay(i1_h1_j1,220,"I1"),
//                new WalkWay(g1_l1,56.61,"L1")
//        };
//        lupton.adjacencies= new WalkWay[]{
//                new WalkWay(hale,317.86,"K1"),
//                new WalkWay(i1_h1_j1,44,"J1")
//        };
//        i1_h1_j1.adjacencies = new WalkWay[]{
//                new WalkWay(greenley,492,"H1"),
//                new WalkWay(lupton,44,"J1"),
//                new WalkWay(roosevelt,220,"I1")
//        };
//        greenley.adjacencies= new WalkWay[]{
//                new WalkWay(b1_c1,74.44,"C1"),
//                new WalkWay(e1_f1_g1,84.7,"F1"),
//                new WalkWay(i1_h1_j1,492,"H1")
//        };
//        vertexes = new Vertex[]{hale,greenley,lupton,roosevelt,a1_b1,b1_c1,d1_e1,e1_f1_g1,g1_l1,i1_h1_j1,};
    }
    public void listenForPath()
    {
        Button find = (Button) findViewById(R.id.findPathButton);
        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vertexes.get(origin.getSelectedItem().toString()) != null && vertexes.get(destination.getSelectedItem().toString()) != null) {
                    Building origin_building = (Building) vertexes.get(origin.getSelectedItem().toString());
                    Building destination_building = (Building) vertexes.get(destination.getSelectedItem().toString());
                    List<Vertex> buildingList = new ArrayList<Vertex>();
                    results = (TextView) findViewById(R.id.resultTextView);

                    if (origin_building.name.equals(destination_building.name)) {
                        buildingList.add(origin_building);
                    } else {
                        Dijkstra.computePaths(origin_building);
                        buildingList = Dijkstra.getShortestPathTo(destination_building);
                    }
                    ArrayList<String> path = new ArrayList<String>();
                    for (Vertex node : buildingList) {
                        path.add((buildingList.indexOf(node) + 1) + " : " + node.getClass().getSimpleName() + " " + node.toString());
                    }
                    route = (ListView) findViewById(R.id.routeListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, path);
                    route.setAdapter(adapter);
                    final List<Vertex> finalBuildingList = buildingList;
                    Toast.makeText(MainActivity.this, "Click on any Building in the list for more information", Toast.LENGTH_LONG).show();
                    route.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            if (finalBuildingList.get(position).getClass().getSimpleName().equalsIgnoreCase("Building")) {
                                Intent intent = new Intent(MainActivity.this, BuildingActivity.class);
                                intent.putExtra("name", finalBuildingList.get(position).getName());
                                startActivity(intent);
                            }

                        }
                    });


                    //results.setText(path);

                    buildGraph(); //TODO: What we need to do is to reset the pointers in the tree. Rebuilding is  more expensive than necessary.

                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id==R.id.login_page)
        {
            Intent intent= new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
        else if(id==R.id.Building_page)
        {
            Intent intent = new Intent(this,BuildingActivity.class);
            startActivity(intent);
        }
        else if(id== R.id.Admin_Home_page)
        {
            Intent intent= new Intent(this,AdminHomepageActivity.class);
            startActivity(intent);
        }
        else if(id== R.id.Edit_Building_page)
        {
            Intent intent = new Intent(this,EditBuildingActivity.class);
            startActivity(intent);
        }
        else if (id==R.id.Department_page)
        {
            Intent intent = new Intent(this, DepartmentActivity.class);
            startActivity(intent);
        }
        else if(id== R.id.Edit_Pathway_page)
        {
            Intent intent = new Intent( this, EditPathwayActivity.class);
            startActivity(intent);
        }
        else if(id== R.id.Schedule_page)
        {
            Intent intent = new Intent (this, ScheduleActivity.class);
            startActivity(intent);
        }
        else if ( id== R.id.Register_page)
        {
            Intent intent = new Intent (this, Register.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            //mLatitudeText.setText(String.valueOf(mLastLocation.getLatitude()));
            //mLongitudeText.setText(String.valueOf(mLastLocation.getLongitude()));
        }
        startLocationUpdates();
    }
    protected void startLocationUpdates() {
        try
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,MainActivity.this);
        }
        catch (Exception $e)
        {
            Log.e("MainActivity-LOG",$e.getMessage()+ "\n"+ $e.toString());
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
       // mLatitudeText.setText(String.valueOf(location.getLatitude()));
       // mLongitudeText.setText(String.valueOf(location.getLongitude()));
    }
}
