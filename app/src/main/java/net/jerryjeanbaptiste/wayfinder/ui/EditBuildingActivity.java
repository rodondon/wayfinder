package net.jerryjeanbaptiste.wayfinder.ui;

//Bryce Schweitzer 2015

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import net.jerryjeanbaptiste.wayfinder.R;
import net.jerryjeanbaptiste.wayfinder.objects.Building;
import net.jerryjeanbaptiste.wayfinder.objects.RealmBuilding;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class EditBuildingActivity extends ActionBarActivity {

    private Realm realm;
    EditText buildingNameET;
    EditText buildingDescriptionET;
    EditText addDepartmentET;
    Spinner buildingSpinner;
    ArrayAdapter<String> buildingListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_building);

         buildingNameET = (EditText) findViewById(R.id.buildingNameEditText);
         buildingDescriptionET = (EditText) findViewById(R.id.buildingDescriptionEditText);


        realm = Realm.getInstance(this);
        loadBuildings();
        listenForRemove();

        listenForSubmit();

    }
    private void listenForRemove()
    {
        Button remove = (Button) findViewById(R.id.removeBuildingButton);
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.beginTransaction();
                String name=buildingListAdapter.getItem(buildingSpinner.getSelectedItemPosition());
                RealmResults<RealmBuilding> to_be_removed = realm.where(RealmBuilding.class).equalTo("BuildingName",name).findAll();
                to_be_removed.clear();
                realm.commitTransaction();
                loadBuildings();
                Toast.makeText(EditBuildingActivity.this,name+" was deleted",Toast.LENGTH_LONG).show();
            }
        });

    }
    private void loadBuildings()
    {
        RealmQuery<RealmBuilding> buildingRealmQuery = realm.where(RealmBuilding.class);
        RealmResults<RealmBuilding> buildingRealmResults= buildingRealmQuery.findAll();
        buildingRealmResults.sort("BuildingName");

        buildingSpinner = (Spinner) findViewById(R.id.buildingsSpinner);

        buildingListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        buildingListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        for(RealmBuilding item : buildingRealmResults)
        {
            buildingListAdapter.add(item.getBuildingName());
        }
        buildingSpinner.setAdapter(buildingListAdapter);
    }
    private void listenForSubmit()
    {
        Button add = (Button) findViewById(R.id.addBuildingButton);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.beginTransaction();
                RealmBuilding building = realm.createObject(RealmBuilding.class);
                building.setBuildingName(buildingNameET.getText().toString());
                building.setBuildingDescription(buildingDescriptionET.getText().toString());
                realm.commitTransaction();
                loadBuildings();
                Toast.makeText(EditBuildingActivity.this,buildingNameET.getText().toString()+" was added",Toast.LENGTH_LONG).show();

            }
        });
    }
    private void loadData(String building)
    {
        RealmResults<RealmBuilding> result= realm.where(RealmBuilding.class).equalTo("BuildingName",building).findAll();
        RealmBuilding found = result.first();
        buildingNameET.setText(found.getBuildingName());
        buildingDescriptionET.setText(found.getBuildingDescription());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_building, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
