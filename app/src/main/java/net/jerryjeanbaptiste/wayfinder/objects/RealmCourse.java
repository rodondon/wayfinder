package net.jerryjeanbaptiste.wayfinder.objects;

import io.realm.RealmObject;

/**
 * Created by Jerry on 4/19/2015.
 */
public class RealmCourse extends RealmObject {
    private String name;
    private String day;
    private int startHour;
    private int startMinute;
    private String period_of_day;
    private String buildingName;
    private String end;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }



    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String building) {
        this.buildingName = building;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(int startMinute) {
        this.startMinute = startMinute;
    }

    public String getPeriod_of_day() {
        return period_of_day;
    }

    public void setPeriod_of_day(String period_of_day) {
        this.period_of_day = period_of_day;
    }
}
