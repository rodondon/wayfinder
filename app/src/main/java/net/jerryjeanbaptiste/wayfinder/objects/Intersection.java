package net.jerryjeanbaptiste.wayfinder.objects;

/**
 * Created by Jerry on 2/14/2015.
 * This is the representation of an intersection.
 * i.e. Where two or more Pathways meet.
 * This class behaves similarly to a Building
 */
public class Intersection extends Vertex {

    public Intersection (RealmVertex vertex)
    {
        super(vertex.getName());
    }
    public Intersection(String argName) {
        super(argName);
    }
}
