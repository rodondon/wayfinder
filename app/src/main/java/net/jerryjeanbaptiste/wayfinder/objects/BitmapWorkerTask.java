package net.jerryjeanbaptiste.wayfinder.objects;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import net.jerryjeanbaptiste.wayfinder.ui.MapActivity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Jerry on 4/11/2015.
 */
public class BitmapWorkerTask extends AsyncTask<Object, Void, BitmapDrawable> {
    private final WeakReference<ImageView> imageViewReference;
    private int data = 0;
    private Context mContext;
    private int mapWidth,mapHeight;

    public BitmapWorkerTask(ImageView imageView, Context context) {
        // Use a WeakReference to ensure the ImageView can be garbage collected
        imageViewReference = new WeakReference<ImageView>(imageView);
        mContext=context;
    }

    // Decode image in background.
    @Override
    protected BitmapDrawable doInBackground(Object... params) {

        data = (int)params[0];
        int map_type=-1;
        if(params.length>1)
            map_type=(int)params[1];

        Bitmap myBitmap= decodeSampledBitmapFromResource(mContext.getResources(), data, 440, 440);
        mapWidth=myBitmap.getWidth();
        mapHeight=myBitmap.getHeight();
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.parseColor("#2222FF"));
        paint.setStrokeWidth (5);
        int x1=0;
        int y1=0;

        Point point = MapActivity.getPoint(40.754108,-73.42827);
        int x2= 100;
        int y2= 200;

        //Create a new image bitmap and attach a brand new canvas to it
        Bitmap tempBitmap = Bitmap.createBitmap(myBitmap.getWidth(), myBitmap.getHeight(), Bitmap.Config.RGB_565);
        Canvas tempCanvas = new Canvas(tempBitmap);
        //Draw the image bitmap into the canvas
        tempCanvas.drawBitmap(myBitmap, 0, 0,null);


        if(map_type==MapActivity.SHOW_REAL_POINT)
        {
            Point point_to_draw = (Point) params[2];
            tempCanvas.drawCircle((float)point_to_draw.getX(),(float) point_to_draw.getY(), 10, paint);

        }
        else if (map_type==MapActivity.SHOW_REAL_PATH)
        {
            ArrayList<Point> points = (ArrayList<Point>) params[2];
            for(int i=0;i<points.size();i++)
            {
                if((i+1)<points.size())
                tempCanvas.drawLine((float)points.get(i).getX(),(float)points.get(i).getY(),(float) points.get(i+1).getX(),(float)points.get(i+1).getY(),paint);
            }
        }
        //tempCanvas.drawLine(x1,y1,x2,y2,paint);
        return new BitmapDrawable(mContext.getResources(), tempBitmap);
    }
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    // Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(BitmapDrawable bitmap) {
        if (imageViewReference != null && bitmap != null) {
            final TouchImageView imageView = (TouchImageView) imageViewReference.get();
            if (imageView != null) {
                imageView.setImageDrawable(bitmap);
            }
        }
    }
}
