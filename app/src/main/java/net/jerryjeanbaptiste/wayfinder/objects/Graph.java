package net.jerryjeanbaptiste.wayfinder.objects;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import net.jerryjeanbaptiste.wayfinder.R;
import net.jerryjeanbaptiste.wayfinder.ui.MapActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by Jerry on 5/10/2015.
 */
public class Graph {
    Realm realm;
    Context mContext;
    HashMap<String, Vertex> vertexes;

    public Graph(Context context) {
        realm = Realm.getInstance(context);
        buildGraph();
    }

    public Graph(Realm realm) {
        this.realm = realm;
        buildGraph();
    }

    public ArrayList<Point> getDrawablePath(Building A, Building B)
    {
        return getDrawablePath(A.getName(),B.getName());
    }
    public ArrayList<Point> getDrawablePath(String building1, String building2)
    {

        Vertex origin_building =  vertexes.get(building1);
        Vertex destination_building =  vertexes.get(building2);
        List<Vertex> buildingList = new ArrayList<Vertex>();
        //results = (TextView) findViewById(R.id.resultTextView);

        ArrayList<Point> path = new ArrayList<Point>();
        if (origin_building.name.equals(destination_building.name)) {
            buildingList.add(origin_building);
        } else {
            Dijkstra.computePaths(origin_building);
            buildingList = Dijkstra.getShortestPathTo(destination_building);
        }

        for (Vertex node : buildingList) {
            RealmVertex result= realm.where(RealmVertex.class).equalTo("name", node.getName()).findFirst();
            path.add(MapActivity.toSmallMap(new Point(result.getX(),result.getY())));

        }
        return path;
    }
    public ArrayList<Point> getDrawableDepBuildPath(String Department, String Building)
    {
        RealmBuilding building = realm.where(RealmBuilding.class).equalTo("departments.name",Department).findFirst();
        return getDrawablePath(building.getBuildingName(),Building);
    }

    public void buildGraph() {
        vertexes = new HashMap<String, Vertex>();
        RealmResults<RealmVertex> list = realm.where(RealmVertex.class).findAll();
        for (RealmVertex vertex : list) {
            if (vertex.isBuilding().equals("true"))
                vertexes.put(vertex.getName(), new Building(vertex));
            else
                vertexes.put(vertex.getName(), new Intersection(vertex));
        }
        for (int i = 0; i < vertexes.size(); i++) {
            RealmList<RealmAdjacency> currentRealmList = list.get(i).getAdjacencies();
            Edge[] adjacencies = new Edge[currentRealmList.size()];
            for (int b = 0; b < currentRealmList.size(); b++) {
                if (vertexes.containsKey(currentRealmList.get(b).getTarget()) && vertexes.get(currentRealmList.get(b).getTarget()) != null)
                    adjacencies[b] = new WalkWay(vertexes.get(currentRealmList.get(b).getTarget()), currentRealmList.get(b).getWeight(), currentRealmList.get(b).getName());
            }
            vertexes.get(list.get(i).getName()).setAdjacencies(adjacencies);
        }
    }
}

