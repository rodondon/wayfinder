package net.jerryjeanbaptiste.wayfinder.objects;

import io.realm.RealmObject;

/**
 * Created by Jerry on 2/14/2015.
 * This is a node in the graph
 */
public class Vertex implements Comparable<Vertex> {
    public final String name;
    public Edge[] adjacencies;
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;

    public Vertex(String argName) {
        name = argName;
    }

    public String toString() {
        return name;
    }

    public int compareTo(Vertex other) {
        return Double.compare(minDistance, other.minDistance);
    }

    public String getName()
    {
        return name;
    }
    public Edge[] getAdjacencies()
    {
        return adjacencies;
    }
    public double getMinDistance()
    {
        return  minDistance;
    }
    public Vertex getPrevious()
    {
        return previous;
    }

    public void setAdjacencies(Edge[] adjacencies) {
        this.adjacencies = adjacencies;
    }

    public void setMinDistance(double minDistance) {
        this.minDistance = minDistance;
    }

    public void setPrevious(Vertex previous) {
        this.previous = previous;
    }
}