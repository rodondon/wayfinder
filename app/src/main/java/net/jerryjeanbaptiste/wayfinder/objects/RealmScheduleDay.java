package net.jerryjeanbaptiste.wayfinder.objects;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by jerry1 on 5/2/15.
 */
public class RealmScheduleDay extends RealmObject {
    private RealmList<RealmCourse> courses;
    private String name;

    public RealmList<RealmCourse> getCourses() {
        return courses;
    }

    public void setCourses(RealmList<RealmCourse> courses) {
        this.courses = courses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
